import React, { Component } from 'react'

import TableComponent from 'component/table-component'
import FormComponent from 'component/form-component'
import InputComponent from 'component/input-component'

class ListContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            members: [],
            groups: [],
            groupName: '',
            groupMembers: [],
            show: false,
            ListValues: {}
        }
        this.handleInputOnChange = this.handleInputOnChange.bind(this)
        this.handleInputOnChange = this.handleAddOnClick.bind(this)
    }

    componentDidMount () {
        fetch('http://localhost:8080/members', { method: 'GET' })
            .then(response => response.json())
            .then(responseObject => {
                this.setState({ members: responseObject })
            })
    }

    handleAddOnClick = () => {
        this.setState({
            ListValues: {},
            show: true
        })
    }

    handleItemOnClick = (event) => {
        const id = event.target.parentElement.id

        fetch('http://localhost:8080/groups/' + id, { method: 'GET' })
            .then(response => response.json())
            .then(responseObject => {
                this.setState({
                    groupMembers: responseObject,
                    show: true
                })
            })
        // fetch('http://localhost:8080/members/' + id, { method: 'GET' })
        //     .then(response => response.json())
        //     .then(responseObject => {
        //         this.setState({
        //             ListValues: responseObject,
        //             show: true
        //         })
        //     })
    }

    handleInputOnChange (event) {
        this.setState({
            ListValues: Object.assign(this.state.ListValues, { [event.target.name]: event.target.value })
        })
    }

    renderList () {
        return (
            <div>
                <TableComponent
                    user={this.state.members}
                    onItemClick={this.handleItemOnClick}
                    onAddClick={this.handleAddOnClick}
                />
            </div>
        )
    }

    renderForm () {
        return (
            <div>
                <h1>Creation d'un group</h1>
                <FormComponent action='/members' onSaveClick={this.handleOnSaveClick}>
                    <InputComponent onChange={this.handleInputOnChange} label='Nom du group:' type='text' name='group' value={this.state.groupName.group} />
                </FormComponent>
            </div>
        )
    }

    render () {
        return (
            <div>
                {this.state.show ? this.renderForm() : this.renderList()}
            </div>
        )
    }
}
export default ListContainer
