import React from 'react'

const FormComponent = ({ children, action, onSaveClick }) => (
    <div>
        <form action={action}>
            {children}
        </form>

        <button onClick={onSaveClick}>sauvegarder</button>
    </div>
)

export default FormComponent
