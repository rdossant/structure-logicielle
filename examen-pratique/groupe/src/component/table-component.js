import React from 'react'

function renderLi (members, onItemClick) {
    return (
        <li key={members.id} id={members.id} title={'username:' + members.id}>
            <span onClick={onItemClick}>
                <strong>{members.userName}</strong> : {members.firstName} {members.lastName}
            </span>
        </li>
    )
}
const TableComponent = ({ members, onItemClick, onAddClick }) => (
    <div>
        <ul>
            {members.map((user) => renderLi(user, onItemClick))}
        </ul>
        <button onClick={onAddClick}>Ajouter</button>
    </div>
)

export default TableComponent
