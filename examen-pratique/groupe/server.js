'use strict'

const express = require('express')
const members = require('./data/members.json')

const app = express()

// parse application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }))

// parse application/json
app.use(express.json())

app.use(express.static('dist'))

// CORS for development
// https://enable-cors.org/server_expressjs.html
app.use(function (request, response, next) {
    response.header('Access-Control-Allow-Origin', '*')
    response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    response.header('Access-Control-Allow-Methods', 'POST, PUT, GET, DELETE, OPTIONS')
    response.header('Access-Control-Allow-Credentials', 'false')
    next()
})
// const members = require('./data/members.json')
const PORT = 8080
const HTTP_OK = 200
const CONTENT_TYPE_JSON = 'application/json'

function writeJSONResponse (request, response, result) {
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(JSON.stringify(result, null, 2))
}

// function findIndex (id) {
//     return members.findIndex(user => user.id === parseInt(id))
// }

app.get('/members', function (request, response) {
    writeJSONResponse(request, response, members)
})
// group
app.get('/groups', function (request, response) {
    writeJSONResponse(request, response, members)
    console.log('sirve')
})

app.post('/groups', function (request, response) {
    members.push(Object.assign, request.body)
    writeJSONResponse(request, response, members)
    console.log('sirve')
})

app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT)
    console.log('connected')
})
