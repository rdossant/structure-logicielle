import React from 'react'

// Cet exercice contient les erreurs suivantes (Le lintage est désactivé)
// Use the `defaultValue` or `value` props on <select> instead of setting `selected` on <option>.
// Warning: Each child in a list should have a unique "key" prop.

function renderOption (option, value) {
    return <option value={option.value} selected={value === option.value}>{option.label}</option>
}

const SelectComponent = ({ text, id, name, value, options }) => (
    <div>
        <label htmlFor={id}>{text}</label>
        <select name={name} id={id}>
            {options.map(option => renderOption(option, value))}
        </select>
    </div>
)

export default SelectComponent
