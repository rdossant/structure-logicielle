import React, { Component } from 'react'

import InputComponent from 'component/input-component'
import SelectComponent from 'component/select-component'
import RadioTitleComponent from 'component/radio-title-component'

const OPTIONS = [{
    label: 'Afghanistan',
    value: '1'
}, {
    label: 'Afrique du Sud',
    value: '2'
}, {
    label: 'Albanie',
    value: '3'
}, {
    label: 'Algérie',
    value: '4'
}]

const TITLES = [{
    label: 'Duc',
    number: 0
}, {
    label: 'Duchesse',
    number: 1
}, {
    label: 'Marquis',
    number: 2
}, {
    label: 'Marquise',
    number: 3
}, {
    label: 'Comte',
    number: 4
}, {
    label: 'Comtesse',
    number: 5
}]

class FormContainer extends Component {
    render () {
        return (
            <div>
                <h1>Formulaire pour l&#39;ajout des usagers</h1>
                <form id='form-test'>
                    <InputComponent
                        text="Nom d'usager:"
                        type='text'
                        id='userName_id'
                        name='userName'
                    />

                    <InputComponent
                        text='Prénom:'
                        type='text'
                        id='firstName_id'
                        name='firstName'
                    />

                    <InputComponent
                        text='Nom:'
                        type='text'
                        id='lastName_id'
                        name='lastName'
                    />

                    <InputComponent
                        text='Âge:'
                        type='number'
                        id='age_id'
                        name='age'
                    />

                    <SelectComponent
                        text='Pay:'
                        id='country_id'
                        name='country'
                        options={OPTIONS}
                    />

                    <RadioTitleComponent
                        legend='Titre'
                        id='title_id'
                        name='title'
                        titles={TITLES}
                    />
                </form>
            </div>
        )
    }
}

export default FormContainer
