import React from 'react'

const buildTitle = (title, name, index) => {
    return (
        <div key={index}>
            <input type='radio' id={'title_id' + title.number} name={name} value={title.number} />
            <label htmlFor={'title_id' + title.number}>{title.label}</label>
        </div>
    )
}
const RadioTitleComponent = ({ id, name, titles }) => (

    <fieldset id={id} name={name}>
        <div>
            {titles.map((title, index) => buildTitle(title, name, index))}
        </div>
    </fieldset>
)

export default RadioTitleComponent
