/**
 * Exercice - JS React table user
 *
 * Objectifs:
 *  - Création d'un tableau avec la librairie React
 *
 * Consignes:
 *  - Créer les fichiers index.js et index.html
 *  - Créer le conteneur table-user-container
 *  - Créer la composante table-user-component pour produire un tableau HTML
 *  - Ajouter le tableau suivant dans table-user-container et le passer en paramètre a table-user-component
 */

const USERS = [{
    firstName: 'Martin',
    lastName: 'Vachon',
    age: 45
}, {
    firstName: 'Julie',
    lastName: 'Turgeon',
    age: 18
}, {
    firstName: 'Mélanie',
    lastName: 'Simard',
    age: 67
}, {
    firstName: 'Pierre',
    lastName: 'Coté',
    age: 34
}, {
    firstName: 'Nadia',
    lastName: 'Beaulieu',
    age: 35
}]
