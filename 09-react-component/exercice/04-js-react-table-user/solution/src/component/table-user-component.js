import React from 'react'

function renderTr (user, index) {
    return (
        <tr key={index}>
            <td>{user.firstName}</td>
            <td>{user.lastName}</td>
            <td>{user.age}</td>
        </tr>
    )
}

const TableUserComponent = ({ users }) => (
    <table>
        <thead>
            <tr>
                <th>First name</th>
                <th>Last name</th>
                <th>Age</th>
            </tr>
        </thead>
        <tbody>
            {users.map((user, index) => renderTr(user, index))}
        </tbody>
    </table>
)

export default TableUserComponent
