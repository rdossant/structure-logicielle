import * as React from 'react'
import * as ReactDOM from 'react-dom'

import TableUserContainer from 'container/table-user-container'

ReactDOM.render(
    <TableUserContainer />,
    document.getElementById('app')
)
