import React from 'react'
function renderOption (option, index, value) {
    return <option value={option.value} key={index} selected={value === option.value}>{option.label}</option>
}

const SelectComponent = ({ text, type, id, name, value, options }) => (
    <div>
        <label htmlFor={id}>{text}</label>
        <select name={name} id={id}>
            {options.map((option, index) => renderOption(option, index, value))}
        </select>
    </div>
)

export default SelectComponent
