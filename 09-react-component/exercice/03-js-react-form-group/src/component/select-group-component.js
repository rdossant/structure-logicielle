import React from 'react'

const buildList = (group, index) => {
    return (
        <optgroup label={group.label} key={index}>
            {group.options.map((option, index) => <option value={option.value} key={index}>{option.label}</option>)}
        </optgroup>
    )
}
const SelectGroupComponent = ({ text, id, name, options }) => (
    <div>
        <label htmlFor={id}>{text}</label>
        <select name={name} id={id}>
            {options.map((group, index) => buildList(group, index))}
        </select>
    </div>
)
export default SelectGroupComponent
