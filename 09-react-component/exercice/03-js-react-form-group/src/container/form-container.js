import React, { Component } from 'react'

import InputComponent from 'component/input-component'
// import SelectComponent from 'component/select-component'
import RadioTitleComponent from 'component/radio-title-component'
import SelectGroupComponent from 'component/select-group-component'

// const OPTIONS = [{
//     label: 'Afghanistan',
//     value: '1'
// }, {
//     label: 'Afrique du Sud',
//     value: '2'
// }, {
//     label: 'Albanie',
//     value: '3'
// }, {
//     label: 'Algérie',
//     value: '4'
// }]

const TITLES = [{
    label: 'Duc',
    number: 0
}, {
    label: 'Duchesse',
    number: 1
}, {
    label: 'Marquis',
    number: 2
}, {
    label: 'Marquise',
    number: 3
}, {
    label: 'Comte',
    number: 4
}, {
    label: 'Comtesse',
    number: 5
}]

const OPTIONS = [{
    label: 'Theropods',
    options: [{
        label: 'Tyrannosaurus',
        number: 0
    }, {
        label: 'Velociraptor',
        number: 1
    }, {
        label: 'Deinonychus',
        number: 2
    }]
}, {
    label: 'Sauropods',
    options: [{
        label: 'Diplodocus',
        number: 3
    }, {
        label: 'Saltasaurus',
        number: 4
    }, {
        label: 'Apatosaurus',
        number: 5
    }]
}]
class FormContainer extends Component {
    render () {
        return (
            <div>
                <h1>Formulaire usager</h1>
                <form id='form-test'>
                    <InputComponent
                        text='First Name:' type='text' id='firstName_id' name='firstName'
                    />
                    <InputComponent
                        text='Last Name:' type='text' id='lastName_id' name='lastName'
                    />
                    <InputComponent
                        text='Age:' type='number' id='age_id' name='age'
                    />
                    {/* <SelectComponent
                        text='Country:' id='country_id' name='country' options={OPTIONS}
                    /> */}
                    <RadioTitleComponent
                        text='Title:' id='title_id' name='title' titles={TITLES}
                    />
                    <SelectGroupComponent
                        text='Choose a dinosaur:'
                        id='group_id'
                        name='group'
                        options={OPTIONS}
                    />
                </form>
            </div>
        )
    }
}
export default FormContainer
