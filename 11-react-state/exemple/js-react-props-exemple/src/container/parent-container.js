import React, { Component } from 'react'

import ChildContainer from 'container/child-container'

class ParentContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            test: 'state-du-parent'
        }
    }

    render () {
        return (
            <div>
                <ChildContainer test={this.state.test} />
            </div>
        )
    }
}

export default ParentContainer
