import React, { Component } from 'react'

class ChildContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
        }
    }

    render () {
        return (
            <div>
                <h1>Child container</h1>
                <div>
                    {this.props.test}
                </div>
            </div>
        )
    }
}

export default ChildContainer
