import React from 'react'
import ReactDOM from 'react-dom'

import ParentContainer from 'container/parent-container'

ReactDOM.render(
    <ParentContainer />,
    document.getElementById('app')
)
