import React, { Component } from 'react'

import InputComponent from 'component/input-component'

class FormContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            formValues: {}
        }

        this.handleInputOnChange = this.handleInputOnChange.bind(this)
    }

    /**
     * Tout les chants du formulaire exécute cette méthode sur l'évènement onChange
     * A chaque charactère entré par l'utilisateur un nouvel objet est créer avec Object.assign()
     * L'objet est créé à partir de l'objet actuel formvalues après avoir assigné les nouvelles valeurs.
     *
     * Exemple de state pour le formulaire.
     * {
     *  userName: 'abc',
     *  firstName: 'b',
     *  lastName: 'c'
     * }
     */
    handleInputOnChange (event) {
        this.setState({
            formValues: Object.assign(this.state.formValues, { [event.target.name]: event.target.value })
        })
    }

    render () {
        return (
            <div>
                <h1>Formulaire</h1>
                <form>
                    <InputComponent onChange={this.handleInputOnChange} label="Nom d'usager:" type='text' id='userName' />

                    <InputComponent onChange={this.handleInputOnChange} label='Prénom:' type='text' id='firstName' />

                    <InputComponent onChange={this.handleInputOnChange} label='Nom:' type='text' id='lastName' />
                </form>

                <h1>État</h1>
                <pre>
                    {JSON.stringify(this.state, null, 4)}
                </pre>
            </div>
        )
    }
}

export default FormContainer
