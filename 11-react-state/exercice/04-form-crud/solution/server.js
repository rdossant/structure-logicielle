'use strict'

const express = require('express')

const app = express()

// parse application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }))

// parse application/json
app.use(express.json())

app.use(express.static('dist'))

// CORS for development
// https://enable-cors.org/server_expressjs.html
app.use(function (request, response, next) {
    response.header('Access-Control-Allow-Origin', '*')
    response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    response.header('Access-Control-Allow-Methods', 'POST, PUT, GET, DELETE, OPTIONS')
    response.header('Access-Control-Allow-Credentials', 'false')
    next()
})

const PORT = 8080
const HTTP_OK = 200
const CONTENT_TYPE_JSON = 'application/json'

// Émulation d'une base de données en utilisant une collection d'objets en mémoire
const USERS = [
    {
        id: 1,
        userName: 'patate',
        firstName: 'Pat',
        lastName: 'Ate'
    },
    {
        id: 2,
        userName: 'gcote',
        firstName: 'Gros',
        lastName: 'Coté'
    },
    {
        id: 3,
        userName: 'fmartineau',
        firstName: 'François',
        lastName: 'Martineau'
    },
    {
        id: 4,
        userName: 'mstpierre',
        firstName: 'Marc',
        lastName: 'St-Pierre'
    },
    {
        id: 5,
        userName: 'msimard',
        firstName: 'Mélanie',
        lastName: 'Simard'
    },
    {
        id: 6,
        userName: 'agermain',
        firstName: 'Audrée',
        lastName: 'Germain'
    }
]

// La valeur du prochain ID pour la création d'un objet à ajouter dans la collection.
let nextId = USERS.length + 1

function writeJSONResponse (request, response, result) {
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(JSON.stringify(result, null, 2))
}

function findIndex (id) {
    return USERS.findIndex(user => user.id === parseInt(id))
}

app.get('/users', function (request, response) {
    writeJSONResponse(request, response, USERS)
})

app.get('/users/:id', function (request, response) {
    const index = findIndex(request.params.id)
    writeJSONResponse(request, response, USERS[index])
})

app.post('/users', function (request, response) {
    // Ajoute dans la collection un nouvel objet sans modifier request.body
    USERS.push(Object.assign({ id: nextId }, request.body))
    // Incrémente le ID
    nextId += 1
    writeJSONResponse(request, response, USERS)
})

app.put('/users', function (request, response) {
    const index = findIndex(request.body.id)
    // Remplace un objet dans la collection sans modifier request.body
    USERS[index] = Object.assign(USERS[index], request.body)
    writeJSONResponse(request, response, USERS)
})

app.delete('/users/:id', function (request, response) {
    const index = findIndex(request.params.id)
    USERS.splice(index, 1)
    writeJSONResponse(request, response, USERS)
})

app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT)
})
