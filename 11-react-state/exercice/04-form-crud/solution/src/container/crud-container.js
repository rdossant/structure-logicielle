import React, { Component } from 'react'

import InputComponent from 'component/input-component'
import ListComponent from 'component/list-component'
import FormComponent from 'component/form-component'

function buildHeader (method, body) {
    return {
        method: method,
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    }
}

class CRUDContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            // Les champs du formulaire
            formValues: {},
            // Indique si le formulaire doit être affiché
            showForm: false,
            // Collection d'objet affiché liste
            users: []
        }

        // Ajustment du contexte d'exécution pour avoir accès à l'instance avec this.
        // Première façon en utilisant .bind(this) et le même résultat est obtenu
        // avec la fonctions fléchée comme dans les exemples suivants.
        this.handleInputOnChange = this.handleInputOnChange.bind(this)
    }

    componentDidMount () {
        fetch('http://localhost:8080/users', { method: 'GET' })
            .then(response => response.json())
            .then(responseObject => {
                this.setState({ users: responseObject })
            })
    }

    /**
     * Tout les chants du formulaire exécute cette méthode sur l'évènement onChange
     * A chaque charactère entré par l'utilisateur un nouvel objet est créer avec Object.assign()
     * L'objet est créé à partir de l'objet actuel formvalues après avoir assigné les nouvelles valeurs.
     *
     * Exemple de state pour le formulaire.
     * Le id est ajouté à la création et les autres propriétés représentent le name et value des balises <input>
     * {
     *  id: 1
     *  userName: 'abc',
     *  firstName: 'b',
     *  lastName: 'c'
     * }
     */
    handleInputOnChange (event) {
        this.setState({
            formValues: Object.assign(this.state.formValues, { [event.target.name]: event.target.value })
        })
    }

    handleItemOnClick = (event) => {
        // Le <span> déclenche l'événement et se trouve à l'intérieur du <li> qui contient l'attribut id
        const id = event.target.parentElement.id

        fetch('http://localhost:8080/users/' + id, { method: 'GET' })
            .then(response => response.json())
            .then(responseObject => {
                this.setState({
                    formValues: responseObject,
                    showForm: true
                })
            })
    }

    handleItemDeleteOnClick = (event) => {
        // Le <button> déclenche l'événement et se trouve à l'intérieur du <li> qui contient l'attribut id
        const id = event.target.parentElement.id

        fetch('http://localhost:8080/users/' + id, { method: 'DELETE' })
            .then(response => response.json())
            .then(response => {
                this.setState({ users: response })
            })
    }

    handleAddOnClick = () => {
        this.setState({
            formValues: {},
            showForm: true
        })
    }

    handleOnSaveClick = () => {
        const method = this.state.formValues.id ? 'PUT' : 'POST'
        fetch('http://localhost:8080/users', buildHeader(method, this.state.formValues))
            .then(response => response.json())
            .then(responseObject => {
                this.setState({
                    users: responseObject,
                    showForm: false
                })
            })
    }

    handleOnCancelClick = () => {
        this.setState({ showForm: false })
    }

    renderForm () {
        return (
            <div>
                <h1>Formulaire</h1>
                <FormComponent action='/users' onSaveClick={this.handleOnSaveClick} onCancelClick={this.handleOnCancelClick}>
                    <InputComponent onChange={this.handleInputOnChange} label="Nom d'usager:" type='text' name='userName' value={this.state.formValues.userName} />

                    <InputComponent onChange={this.handleInputOnChange} label='Prénom:' type='text' name='firstName' value={this.state.formValues.firstName} />

                    <InputComponent onChange={this.handleInputOnChange} label='Nom:' type='text' name='lastName' value={this.state.formValues.lastName} />
                </FormComponent>
            </div>
        )
    }

    renderList () {
        return (
            <div>
                <h1>Liste</h1>
                <ListComponent
                    users={this.state.users}
                    onItemClick={this.handleItemOnClick}
                    onItemDeleteClick={this.handleItemDeleteOnClick}
                    onAddClick={this.handleAddOnClick}
                />
            </div>
        )
    }

    render () {
        return (
            <div>
                {this.state.showForm ? this.renderForm() : this.renderList()}
            </div>
        )
    }
}

export default CRUDContainer
