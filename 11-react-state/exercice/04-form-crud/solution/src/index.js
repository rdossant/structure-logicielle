import React from 'react'
import ReactDOM from 'react-dom'

import CRUDContainer from 'container/crud-container'

ReactDOM.render(
    <CRUDContainer />,
    document.getElementById('app')
)
