
const ARRAY_NUMBER = [0, 1, 2, 3, 4]

// previousValue
//  La valeur précédemment retournée par le dernier appel du callback, ou valeurInitiale, si elle est fournie.

// initialValue
//  Une valeur utilisée comme premier argument lors du premier appel de la fonction callback.
//  Si aucune valeur initiale n'est fournie, le premier élément du tableau est utilisé
//  (et la boucle de traitement ne le parcourera pas)


console.log('Sans initialValue le premier élément n\'est pas parcouru')
ARRAY_NUMBER.reduce(function (previousValue, currentValue, currentIndex, array) {
    console.log(currentValue)
    return previousValue + currentValue
})

// Résultat
// 1
// 2
// 3
// 4

console.log('Avec un initialValue le premier élément sera parcouru')
const initialValue = 0
ARRAY_NUMBER.reduce(function (previousValue, currentValue, currentIndex, array) {
    console.log(currentValue)
    return previousValue + currentValue
}, initialValue)

// Résultat
// 0
// 1
// 2
// 3
// 4