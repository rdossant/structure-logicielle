'use strict'

const ObjetFonction = function () {
    const privateVariable = 'privateVariable'

    function privateFunction () {
        return 'privateVariable'
    }

    return {
        publicVariable: 'publicVariable',

        getPrivateVariable: function () {
            return privateVariable
        },

        executePrivateFunction: function () {
            return privateFunction()
        }
    }
}

const objetFunction = new ObjetFonction()

console.log(objetFunction.getPrivateVariable())
console.log(objetFunction.publicVariable)
console.log(objetFunction.executePrivateFunction())
