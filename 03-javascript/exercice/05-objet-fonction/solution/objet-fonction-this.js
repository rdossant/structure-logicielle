'use strict'

const ObjetFonction = function () {
    const privateVariable = 'val1'

    function privateFunction () {
        return 'val3'
    }

    this.publicVariable = 'val2'

    this.getPrivateVariable = function () {
        return privateVariable
    }

    this.executePrivateFunction = function () {
        return privateFunction()
    }
}

const objetFunction = new ObjetFonction()
console.log(objetFunction.publicVariable)
console.log(objetFunction.getPrivateVariable())
console.log(objetFunction.executePrivateFunction())
