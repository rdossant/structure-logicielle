'use strict'

const ObjetFonction = function () {
    const privateVariable = 'val1'

    function privateFunction () {
        return 'val3'
    }

    return {
        publicVariable: 'val2',

        getPrivateVariable: function () {
            return privateVariable
        },

        executePrivateFunction: function () {
            return privateFunction()
        }
    }
}

const objetFunction = new ObjetFonction()
console.log(objetFunction.publicVariable)
console.log(objetFunction.getPrivateVariable())
console.log(objetFunction.executePrivateFunction())
