'use strict'
const USER_ARRAY = [
    { id: 1, name: 'Martin', age: 45 },
    { id: 2, name: 'Pierre', age: 15 },
    { id: 3, name: 'Josée', age: 14 },
    { id: 4, name: 'Melanie', age: 32 },
    { id: 5, name: 'Sonia', age: 24 }
]

/* Résultat 1 */

const resultat1 = USER_ARRAY.map(function (item) {
    return {
        id: item.id,
        name: item.name
    }
})

console.log(resultat1)

/* Résultat 2 */

const resultat2 = USER_ARRAY.filter(function (item) {
    return item.age > 15
})

console.log(resultat2)

/* Résultat 3 */

const valInitial = 0
const resultat3 = USER_ARRAY.reduce(function (accumulateur, value, index, array) {
    return accumulateur + value.age
}, valInitial)

console.log(resultat3 / USER_ARRAY.length)
