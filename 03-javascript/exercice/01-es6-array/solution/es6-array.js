'use strict'

const USER_ARRAY = [
    { id: 1, name: 'Martin', age: 45 },
    { id: 2, name: 'Pierre', age: 15 },
    { id: 3, name: 'Josée', age: 14 },
    { id: 4, name: 'Melanie', age: 32 },
    { id: 5, name: 'Sonia', age: 24 }
]

const mapResult = USER_ARRAY.map(function (item) {
    return {
        id: item.id,
        name: item.name
    }
})

console.log(mapResult)

const filterResult = USER_ARRAY.filter(function (item) {
    return item.age > 15
})

console.log(filterResult)

const initialValue = 0
const reduceResult = USER_ARRAY.reduce(function (accumulator, currentValue, currentIndex, array) {
    return accumulator + currentValue.age
}, initialValue)

console.log(reduceResult / USER_ARRAY.length)

