'use strict'

class ObjetClasse {
    static staticFunction () {
        return 'stattic function'
    }

    publicFunction () {
        return 'public function'
    }

    executePrivateFunction () {
        return privateFunction
    }
}

function privateFunction () {
    return 'fonction private'
}

const objetClasse = new ObjetClasse()
console.log(ObjetClasse.staticFunction())
console.log(objetClasse.publicFunction())
console.log(objetClasse.executePrivateFunction())
