'use strict'

function privateFunction () {
    return 'Value from private function'
}

class ObjetClasse {
    constructor (paramA) {
        this.paramA = paramA
    }

    static staticFunction () {
        return 'public static function result'
    }

    publicFunction () {
        return 'public function result'
    }

    executePrivateFunction () {
        return privateFunction()
    }
}

const objetClass = new ObjetClasse('valA')
console.log(objetClass.publicFunction())
console.log(objetClass.executePrivateFunction())
// Attention: cette méthode est appelée sur la classe et non sur l'instance
console.log(ObjetClasse.staticFunction())
