'use strict'
/* Implémenter les méthodes suivantes:
 *  - readDatas (fileName)
 *  - readData (fileName, id)
 *  - saveDatas (fileName, datas)
 *  - addData (fileName, data)
 *  - updateData (fileName, data) */

const fs = require('fs')
// const path = require('path')

function saveDatas (fileName, datas) {
    const string = JSON.stringify(datas)
    fs.writeFileSync(fileName, string)
    // console.log('----------------------')
    // console.log('Save Data-- Dans Save Datas')
}

function readDatas (fileName) {
    const datasString = fs.readFileSync(fileName)
    return JSON.parse(datasString)
}

function readData (fileName, id) {
    const datas = readDatas(fileName)
    const identifiant = findIndex(datas, id)
    return datas[identifiant]
}

function findIndex (datas, id) {
    const identifiant = datas.findIndex(function (element) {
        return element.id === id
    })

    if (identifiant === -1) {
        throw new Error('Element not found')
    }
    return identifiant
}

function addData (fileName, data) {
    const datas = readDatas(fileName)
    datas.forEach(element => {
        if (element.id === data.id) {
            throw new Error('Element already exists')
        }
    })
    // console.log('----------------------')
    // console.log('Save dans addData')
    saveDatas(fileName, datas)
}

function updateData (fileName, data) {
    const datas = readDatas(fileName)
    const identifiant = findIndex(datas, data.id)
    datas[identifiant] = data
    saveDatas(fileName, datas)
    /* console.log('----------------------')
    console.log('Save data dans update')
    console.log('Update data') */
}

module.exports = {
    readDatas,
    readData,
    saveDatas,
    addData,
    updateData
}
