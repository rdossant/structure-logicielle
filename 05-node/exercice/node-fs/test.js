'use strict'

// const fs = require('fs')
const nodeFs = require('.')
const assert = require('assert').strict

const TEST_FILE_NAME = 'test.json'

const TEST_DATA = [
    { id: 100, userName: 'mvachon', age: 12 },
    { id: 101, userName: 'jcote', age: 66 },
    { id: 102, userName: 'pmartineau', age: 99 }
]
nodeFs.saveDatas(TEST_FILE_NAME, TEST_DATA)
assert.deepStrictEqual(nodeFs.readDatas(TEST_FILE_NAME), TEST_DATA)
nodeFs.addData(TEST_FILE_NAME, { id: 103, userName: 'rdossant', age: 18 })
nodeFs.updateData(TEST_FILE_NAME, { id: 103, userName: 'rdossant', age: 21 })
