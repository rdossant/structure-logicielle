import React, { Component } from 'react'

import ListItemComponent from 'component/list-item-component'

class ListContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            users: []
        }
    }

    componentDidMount () {
        fetch('http://localhost:8080/users', { method: 'GET' })
            .then(response => response.json())
            .then(response => {
                this.setState({ users: response })
            })
    }

    handleClick = (index) => (event) => {
        console.log('this', this)
        console.log(index)
        console.log(event)
    }

    buildList (users) {
        return users.map((user, index) => <ListItemComponent text={user.userName} onClick={this.handleClick(index)} key={index} />)
    }

    render () {
        const users = this.state.users

        return (
            <div>
                <h1>Liste d&#39;usager ({users.length})</h1>
                <ul>
                    {users.length > 0 ? this.buildList(users) : <li>Aucun usager trouvé ...</li>}
                </ul>
            </div>
        )
    }
}

export default ListContainer
