import React from 'react'

const ListItemComponent = ({ text, onClick }) => (
    <li onClick={onClick}>{text}</li>
)

export default ListItemComponent
