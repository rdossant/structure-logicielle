'use strict'

const express = require('express')

const app = express()
app.use(express.static('dist'))

// CORS for development
// https://enable-cors.org/server_expressjs.html
app.use(function (request, response, next) {
    response.header('Access-Control-Allow-Origin', '*')
    response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    response.header('Access-Control-Allow-Methods', 'POST, PUT, GET, DELETE, OPTIONS')
    response.header('Access-Control-Allow-Credentials', 'false')
    next()
})

const PORT = 8080
const HTTP_OK = 200
const CONTENT_TYPE_JSON = 'application/json'

const USERS = [
    {
        userName: 'mvachon'
    },
    {
        userName: 'patate'
    },
    {
        userName: 'gcote'
    },
    {
        userName: 'fmartineau'
    },
    {
        userName: 'mstpierre'
    },
    {
        userName: 'msimard'
    },
    {
        userName: 'agermain'
    }
]

app.get('/users', function (request, response) {
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(JSON.stringify(USERS, null, 4))
})

app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT)
})
