
import React, { Component } from 'react'

import ListItemComponent from 'component/list-item-component'

class ListContainer extends Component {
    constructor () {
        super()

        this.state = {
            users: []
        }

        this.handleClick = (index) => (event) => this.handleClick(event, index)
    }

    componentDidMount () {
        fetch('user-list.json', { method: 'GET' })
            .then(response => response.json())
            .then(response => {
                this.setState({ users: response })
            })
    }

    render () {
        return (
            <div>
                <h1>Liste d'usager</h1>
                <ul>
                    {this.state.users.map((user, index) => <ListItemComponent key={index} text={user.userName} />)}
                </ul>
            </div>
        )
    }

    /** consolelog para presentar en la consola del navegador */
    handleClick (event, index) {
        console.log('this', this)
        console.log('index', index)
        console.log('event', event)
    }
buildLi(item){
    return(
        item.map((user, index) => <ListItemComponent text={user.userName} onClick={this.handleClick(index)} key={index}/>
    ),
}
//  users.map((user, index) => <ListItemComponent text={user.userName} onClick={this.handleClick(index)} key={index} />
}
export default ListContainer
