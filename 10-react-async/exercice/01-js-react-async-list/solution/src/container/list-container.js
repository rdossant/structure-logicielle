import React, { Component } from 'react'

import ListItemComponent from 'component/list-item-component'

class ListContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            users: []
        }

        // Ajustement du contexte d'exécution avec .bind() pour avoir accès à this
        this.handleClick = this.handleClick.bind(this)

        // Déclaration d'une fonction qui retourne une fonction
        // Note: Une déclaration anonyme de cette fonction dans le render() crée une nouvelle fonction à chaque exécution de la méthode.
        this.handleClick2 = (index) => (event) => this.handleClick(event, index)
    }

    componentDidMount () {
        fetch('user-list.json', { method: 'GET' })
            .then(response => response.json())
            .then(response => {
                this.setState({ users: response })
            })
    }

    handleClick (event, index) {
        console.log('this', this)
        console.log('INDEX', index)
        console.log('EVENT:', event)

        // <li data-number={number} onClick={onClick}>{text}</li>
        console.log('Exemple supplémentaire utilisant un data attribute:', event.target.getAttribute('data-number'))
    }

    handleClick3 = (index) => (event) => {
        console.log('this', this)
        console.log('INDEX', index)
        console.log('EVENT:', event)
    }

    buildList (users) {
        // 1) Exemple d'usage avec .bind() utilisé dans le constructeur.
        // Cette façon ne permet pas de fournir des paramètres supplémentaires.
        // L'usage du id="" ou data-*="" pour contenir les paramètres est simple a utiliser avec event.target
        return users.map((user, index) => <ListItemComponent text={user.userName} onClick={this.handleClick} key={index} number={index} />)

        // 2) Exemple avec une fonction fléchée utilisé dans le constructeur
        // return users.map((user, index) => <ListItemComponent text={user.userName} onClick={this.handleClick2(index)} key={index} />)

        // 3) Exemple ayant tous les avantages et utilise la syntaxe la plus moderne
        // return users.map((user, index) => <ListItemComponent text={user.userName} onClick={this.handleClick3(index)} key={index} />)
    }

    render () {
        const users = this.state.users

        return (
            <div>
                <h1>Liste d&#39;usager ({users.length})</h1>
                <ul>
                    {users.length > 0 ? this.buildList(users) : <li>Aucun usager trouvé ...</li>}
                </ul>
            </div>
        )
    }
}

export default ListContainer
