import React from 'react'

const ListItemComponent = ({ text, onClick, number }) => (
    <li data-number={number} onClick={onClick}>{text}</li>
)

export default ListItemComponent
